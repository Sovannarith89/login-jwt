'use client';

import {useState} from "react";

export default function TestJWT() {

    const [accessToken, setAccessToken] = useState("");
    const [user, setUser] = useState(null);
    const [unAuthorized, setUnAuthorized] = useState(false);

    // Handle Login
    const handleLogin = async () => {
        const email = "hout.sovannarith2000@gmail.com";
        const password = "@Admin1234";

        fetch(process.env.NEXT_PUBLIC_API_URL + "/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({email, password}),
        }).then((res) => res.json()).then((data) => {
                console.log("Response When Login", data);
                if (data.accessToken) {
                    setAccessToken(data.accessToken);
                    setUser(data.user);
                }
            }
        ).catch((error) => {
            console.error('Login error:', error);
        });
    }

    // Handle Partial Update
    const handlePartialUpdate = async () => {
        const body = {
            name: "Update by Sovannarith Hout",
        };

        const res = await fetch(`${process.env.NEXT_PUBLIC_DJANGO_API_URL}/api/products/${294}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${accessToken}`,
            },
            body: JSON.stringify(body),
        })
        if(res.status === 401){
            setUnAuthorized(true);
        }

        const data = await res.json();
        console.log("Data from partial update", data);
    }

    // Handle Refresh Token
    const handleRefreshToken = async () => {
        fetch(process.env.NEXT_PUBLIC_API_URL + "/refresh", {
            method: "POST",
            credentials: "include",
            body: JSON.stringify({}),
        }).then((res) => res.json()).then((data) => {
            setAccessToken(data.accessToken)
            console.log("Data from refresh token", data);
        }).catch((error) => {
            console.log(error)
            if (error.status === 401) {
                setUnAuthorized(true);
            }
        });
    }
    return (
        <main className="h-screen grid place-content-center">
            <h2 className="text-5xl">Test Handle Login</h2>
            <button
                onClick={handleLogin}
                className="my-4 p-4 bg-blue-600 rounded-xl text-3xl text-gray-100">Login
            </button>
            <button
                onClick={handlePartialUpdate}
                className="my-4 p-4 bg-blue-600 rounded-xl text-3xl text-gray-100">Handle Partial Update
            </button>
            {unAuthorized && (
                <button
                    onClick={handleRefreshToken}
                    className="my-4 p-4 bg-blue-600 rounded-xl text-3xl text-gray-100">Refresh Token
                </button>
            )}
        </main>
    )
}