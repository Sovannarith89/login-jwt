import {serialize} from "cookie";
import {cookies} from "next/headers";
import {NextResponse} from "next/server";

export async function POST() {
    const cookieStore = cookies();
    const cookieName = process.env.COOKIE_REFRESH_TOKEN_NAME || "refresh_token";
    const credentials = cookieStore.get(cookieName);

    if (!credentials) {
        return NextResponse.json({
            message: "Token not found",
        }, {
            status: 404,
        })
    }

    const refreshToken = credentials.value;

    const response = await fetch(`${process.env.DJANGO_API_URL}/api/token/refresh/`,
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({refresh: refreshToken}),
        }
    );

    if (!response.ok) {
        return NextResponse.json({
            message: "Failed to refresh token",
        }, {
            status: response.status,
        })
    }

    const data = await response.json();
    const refresh = data?.refresh || null;
    const access = data?.access || null;

    const serialized = serialize(cookieName, refresh, {
        httpOnly: true,
        secure: process.env.NODE_ENV === "production",
        path: "/",
        sameSite: "lax",
    });

    return NextResponse.json(
        {
            accessToken: access,
        },
        {
            headers: {
                "Set-Cookie": serialized,
            },
        }
    );

}